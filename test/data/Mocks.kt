package com.tpnlabs.labellingo.data

import com.tpnlabs.labellingo.data.MockUUIDs.userList
import com.tpnlabs.labellingo.models.user.User

object MockUUIDs {
    val userList = listOf(
        "21611e7c-3966-4720-946b-4351d4d9da32",
        "9931f4f6-ce12-465b-8a10-ad35e7fbbbae",
        "08743801-250d-4a80-9578-630afd094689"
    )
}

object MockData {
    val newUser: User.Register = User.Register(
        userList[0],
        "mock@email.com",
        "mockUsername",
        "mock-pass"
    )
}
