package com.tpnlabs.labellingo.models.user

import com.tpnlabs.labellingo.config.DbContstants.LARGE_STRING_LENGTH
import com.tpnlabs.labellingo.config.DbContstants.MEDIUM_STRING_LENGTH
import com.tpnlabs.labellingo.config.DbContstants.STRING_LENGTH
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.LocalDateTime
import java.util.*

object UsersTable : UUIDTable("users") {
    val firstName: Column<String> = varchar("first_name", STRING_LENGTH).default("")
    val lastName: Column<String> = varchar("last_name", STRING_LENGTH).default("")
    val username: Column<String> = varchar("username", STRING_LENGTH).uniqueIndex()
    val role: Column<Int> = integer("role")
    val profileDescription: Column<String> = text("profile_description").default("")
    var email: Column<String> = varchar("email", MEDIUM_STRING_LENGTH).uniqueIndex()
    val emailVerifiedAt: Column<String?> = varchar("email_verified_at", MEDIUM_STRING_LENGTH).nullable()
    val emailToken: Column<String> = varchar("email_token", MEDIUM_STRING_LENGTH).default("")
    var password: Column<String> = varchar("password", LARGE_STRING_LENGTH)
    val createdAt: Column<LocalDateTime> = datetime("created_at").default(LocalDateTime.now())
}

class User(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<User>(UsersTable)

    var firstName by UsersTable.firstName
    var lastName by UsersTable.lastName
    var username by UsersTable.username
    var role by UsersTable.role
    var profileDescription by UsersTable.profileDescription
    var email by UsersTable.email
    var emailVerifiedAt by UsersTable.emailVerifiedAt
    var emailToken by UsersTable.emailToken
    var password by UsersTable.password
    var createdAt by UsersTable.createdAt

    data class UpdateAdmin(
        val id: String,
        val email: String,
        val username: String,
        val role: Int,
        val premium: Int,
    )

    data class Login(
        val user: String,
        val password: String,
    )

    data class Page(
        val id: String,
        val email: String,
        val username: String,
        val role: Int,
        val createdAt: LocalDateTime,
    ) {
        companion object {
            fun fromUserRow(row: User): Page = Page(
                id = row.id.toString(),
                email = row.email,
                username = row.username,
                role = row.role,
                createdAt = row.createdAt,
            )
        }
    }

    data class Register(
        val id: String?,
        val email: String,
        val username: String,
        val password: String?,
    )

    data class Response(
        val id: String,
        val username: String,
    )
}
