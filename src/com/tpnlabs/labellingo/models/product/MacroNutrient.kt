package com.tpnlabs.labellingo.models.product

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.LocalDateTime
import java.util.*

object MacroNutrientsTable : UUIDTable("macro_nutrients") {
    val productId: Column<EntityID<UUID>?> = optReference(
        "product_id",
        ProductsTable,
        onDelete = ReferenceOption.SET_NULL
    )
    val carbohydrates: Column<Double> = double("carbohydrates_value").default(0.0)
    val energyKcal: Column<Double> = double("energy_kcal_value").default(0.0)
    val energyKJ: Column<Double> = double("energy_kj_value").default(0.0)
    val fat: Column<Double> = double("fat_value").default(0.0)
    val proteins: Column<Double> = double("proteins_value").default(0.0)
    val salt: Column<Double> = double("salt_value").default(0.0)
    val saturatedFats: Column<Double> = double("saturated_fats_value").default(0.0)
    val sugars: Column<Double> = double("sugars_value").default(0.0)
    val createdAt: Column<LocalDateTime> = datetime("created_at").default(LocalDateTime.now())
}

class MacroNutrient(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<MacroNutrient>(MacroNutrientsTable)

    var productId by MacroNutrientsTable.productId
    var carbohydrates by MacroNutrientsTable.carbohydrates
    var energyKcal by MacroNutrientsTable.energyKcal
    var energyKJ by MacroNutrientsTable.energyKJ
    var fat by MacroNutrientsTable.fat
    var proteins by MacroNutrientsTable.proteins
    var salt by MacroNutrientsTable.salt
    var saturatedFats by MacroNutrientsTable.saturatedFats
    var sugars by MacroNutrientsTable.sugars
    var createdAt by MacroNutrientsTable.createdAt

    data class Upsert(
        val id: String?,
        val carbohydrates: Double,
        val energyKcal: Double,
        val energyKJ: Double,
        val fat: Double,
        val proteins: Double,
        val salt: Double,
        val saturatedFats: Double,
        val sugars: Double,
        val createdAt: LocalDateTime,
    )

    data class Details(
        val id: String?,
        val product: Product.Response,
        val carbohydrates: Double,
        val energyKcal: Double,
        val energyKJ: Double,
        val fat: Double,
        val proteins: Double,
        val salt: Double,
        val saturatedFats: Double,
        val sugars: Double,
        val createdAt: LocalDateTime,
    ) {
        companion object {
            fun fromRow(row: MacroNutrient): Details {
                val productDb = Product.findById((row.productId!!))!!

                return Details(
                    id = row.id.toString(),
                    product = Product.Response.fromRow(productDb),
                    carbohydrates = row.carbohydrates,
                    energyKcal = row.energyKcal,
                    energyKJ = row.energyKJ,
                    fat = row.fat,
                    proteins = row.proteins,
                    salt = row.salt,
                    saturatedFats = row.saturatedFats,
                    sugars = row.sugars,
                    createdAt = row.createdAt,
                )
            }
        }
    }

    data class Response(
        val id: String,
        val energyKcal: Double,
    ) {
        companion object {
            fun fromRow(row: MacroNutrient): Response {
                return Response(
                    id = row.id.toString(),
                    energyKcal = row.energyKcal,
                )
            }
        }
    }
}
