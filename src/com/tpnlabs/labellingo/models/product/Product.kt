package com.tpnlabs.labellingo.models.product

import com.tpnlabs.labellingo.config.DbContstants.LARGE_STRING_LENGTH
import com.tpnlabs.labellingo.config.DbContstants.MEDIUM_STRING_LENGTH
import com.tpnlabs.labellingo.config.DbContstants.STRING_LENGTH
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.javatime.date
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

object ProductsTable : UUIDTable("products") {
    val distributorId: Column<EntityID<UUID>?> = optReference(
        "distributor_id",
        DistributorsTable,
        onDelete = ReferenceOption.SET_NULL
    )
    val name: Column<String> = varchar("name", LARGE_STRING_LENGTH).default("")
    val brandName: Column<String> = varchar("brand_name", MEDIUM_STRING_LENGTH).default("")
    val barcode: Column<String> = varchar("barcode", STRING_LENGTH).default("")
    val weightValue: Column<Double> = double("weight_value").default(0.0)
    val weightType: Column<String> = varchar("weight_type", STRING_LENGTH).default("")
    val producedAt: Column<LocalDate> = date("produced_at").default(LocalDate.now())
    val expiresAt: Column<LocalDate> = date("expires_at").default(LocalDate.now())
    val createdAt: Column<LocalDateTime> = datetime("created_at").default(LocalDateTime.now())
}

class Product(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<Product>(ProductsTable)

    var distributorId by ProductsTable.distributorId
    var name by ProductsTable.name
    var brandName by ProductsTable.brandName
    var barcode by ProductsTable.barcode
    var weightValue by ProductsTable.weightValue
    var weightType by ProductsTable.weightType
    var producedAt by ProductsTable.producedAt
    var expiresAt by ProductsTable.expiresAt
    var createdAt by ProductsTable.createdAt

    data class Upsert( // Upsert = Update + Insert
        val id: String?,
        val distributorId: String,
        val name: String,
        val brandName: String,
        val barcode: String,
        val weightValue: Double,
        val weightType: String,
        val producedAt: String,
        val expiresAt: String,
    )

    data class Details(
        val id: String,
        val distributor: Distributor.Response,
        val name: String,
        val brandName: String,
        val barcode: String,
        val weightValue: Double,
        val weightType: String,
        val producedAt: LocalDate,
        val expiresAt: LocalDate,
        val createdAt: LocalDateTime,
    ) {
        companion object {
            fun fromRow(row: Product): Details {
                val distributorDb = Distributor.findById((row.distributorId!!))!!

                return Details(
                    id = row.id.toString(),
                    distributor = Distributor.Response.fromRow(distributorDb),
                    name = row.name,
                    brandName = row.brandName,
                    barcode = row.barcode,
                    weightValue = row.weightValue,
                    weightType = row.weightType,
                    producedAt = row.producedAt,
                    expiresAt = row.expiresAt,
                    createdAt = row.createdAt,
                )
            }
        }
    }

    data class Response(
        val id: String,
        val name: String,
    ) {
        companion object {
            fun fromRow(row: Product): Response {
                return Response(
                    id = row.id.toString(),
                    name = row.name,
                )
            }
        }
    }
}
