package com.tpnlabs.labellingo.models.product

import com.tpnlabs.labellingo.config.DbContstants.LARGE_STRING_LENGTH
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.LocalDateTime
import java.util.*

object IngredientsTable : UUIDTable("ingredients") {
    val productId: Column<EntityID<UUID>?> = optReference(
        "product_id",
        ProductsTable,
        onDelete = ReferenceOption.SET_NULL
    )
    val name: Column<String> = varchar("name", LARGE_STRING_LENGTH).default("")
    val percentage: Column<Double> = double("percentage").default(0.0)
    val healthy: Column<Boolean> = bool("healthy").default(false)
    val createdAt: Column<LocalDateTime> = datetime("created_at").default(LocalDateTime.now())
}

class Ingredient(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<Ingredient>(IngredientsTable)

    var productId by IngredientsTable.productId
    var name by IngredientsTable.name
    var percentage by IngredientsTable.percentage
    var healthy by IngredientsTable.healthy
    var createdAt by IngredientsTable.createdAt

    data class Upsert(
        val id: String?,
        val name: String,
        val percentage: Double,
        val healthy: Boolean,
        val createdAt: LocalDateTime,
    )

    data class Details(
        val id: String?,
        val product: Product.Response,
        val name: String,
        val percentage: Double,
        val healthy: Boolean,
        val createdAt: LocalDateTime,
    ) {
        companion object {
            fun fromRow(row: Ingredient): Details {
                val productDb = Product.findById((row.productId!!))!!

                return Details(
                    id = row.id.toString(),
                    product = Product.Response.fromRow(productDb),
                    name = row.name,
                    percentage = row.percentage,
                    healthy = row.healthy,
                    createdAt = row.createdAt,
                )
            }
        }
    }

    data class Response(
        val id: String,
        val name: String,
    ) {
        companion object {
            fun fromRow(row: Ingredient): Response {
                return Response(
                    id = row.id.toString(),
                    name = row.name,
                )
            }
        }
    }
}
