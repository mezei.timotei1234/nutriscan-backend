package com.tpnlabs.labellingo.models.product

import com.tpnlabs.labellingo.config.DbContstants.LARGE_STRING_LENGTH
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.LocalDateTime
import java.util.*

object MicroNutrientsTable : UUIDTable("micro_nutrients") {
    val productId: Column<EntityID<UUID>?> = optReference(
        "product_id",
        ProductsTable,
        onDelete = ReferenceOption.SET_NULL
    )
    val vitamins: Column<String> = varchar("vitamins", LARGE_STRING_LENGTH).default("")
    val minerals: Column<String> = varchar("minerals", LARGE_STRING_LENGTH).default("")
    val createdAt: Column<LocalDateTime> = datetime("created_at").default(LocalDateTime.now())
}

class MicroNutrient(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<MicroNutrient>(MicroNutrientsTable)

    var productId by MicroNutrientsTable.productId
    var vitamins by MicroNutrientsTable.vitamins
    var minerals by MicroNutrientsTable.minerals
    var createdAt by MicroNutrientsTable.createdAt

    data class Upsert(
        val id: String?,
        val vitamins: String,
        val minerals: String,
        val createdAt: LocalDateTime,
    )

    data class Details(
        val id: String?,
        val product: Product.Response,
        val vitamins: String,
        val minerals: String,
        val createdAt: LocalDateTime,
    ) {
        companion object {
            fun fromRow(row: MicroNutrient): Details {
                val productDb = Product.findById((row.productId!!))!!

                return Details(
                    id = row.id.toString(),
                    product = Product.Response.fromRow(productDb),
                    vitamins = row.vitamins,
                    minerals = row.minerals,
                    createdAt = row.createdAt,
                )
            }
        }
    }

    data class Response(
        val id: String,
        val vitamins: String,
    ) {
        companion object {
            fun fromRow(row: MicroNutrient): Response {
                return Response(
                    id = row.id.toString(),
                    vitamins = row.vitamins,
                )
            }
        }
    }
}
