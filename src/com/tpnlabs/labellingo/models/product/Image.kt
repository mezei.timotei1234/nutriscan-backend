package com.tpnlabs.labellingo.models.product

import com.tpnlabs.labellingo.config.DbContstants.ImageType
import com.tpnlabs.labellingo.config.NumberConstants.SEVEN
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.javatime.datetime
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import java.time.LocalDateTime
import java.util.UUID

object ImagesTable : UUIDTable("images") {
    val productId: Column<EntityID<UUID>?> = optReference(
        "product_id",
        ProductsTable,
        onDelete = ReferenceOption.SET_NULL
    )
    val imageData: Column<ExposedBlob> = blob("image_data")
    val imageType: Column<ImageType> = enumerationByName("image_type", SEVEN, ImageType::class)
    val createdAt: Column<LocalDateTime> = datetime("created_at").default(LocalDateTime.now())
}

class Image(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<Image>(ImagesTable)

    var productId by ImagesTable.productId
    var imageData by ImagesTable.imageData
    var imageType by ImagesTable.imageType
    var createdAt by ImagesTable.createdAt

    data class Upsert(
        val id: String?,
        val productId: String,
        val imageData: ExposedBlob,
        val imageType: ImageType,
        val createdAt: LocalDateTime,
    )

    data class Details(
        val id: String,
        val product: Product.Response,
        val imageData: ExposedBlob,
        val imageType: ImageType,
        val createdAt: LocalDateTime,
    ) {
        companion object {
            fun fromRow(row: Image): Details {
                val productDb = Product.findById((row.productId!!))!!

                return Details(
                    id = row.id.toString(),
                    product = Product.Response.fromRow(productDb),
                    imageData = row.imageData,
                    imageType = row.imageType,
                    createdAt = row.createdAt,
                )
            }
        }
    }

    data class Response(
        val id: String,
        val type: ImageType,
    ) {
        companion object {
            fun fromRow(row: Image): Response {
                return Response(
                    id = row.id.toString(),
                    type = row.imageType,
                )
            }
        }
    }
}
