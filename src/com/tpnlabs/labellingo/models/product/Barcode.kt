package com.tpnlabs.labellingo.models.product

import com.tpnlabs.labellingo.config.DbContstants.BarcodeType
import com.tpnlabs.labellingo.config.DbContstants.LARGE_STRING_LENGTH
import com.tpnlabs.labellingo.config.NumberConstants.FIVE
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.LocalDateTime
import java.util.UUID

object BarcodesTable : UUIDTable("consume") {
    val productId: Column<EntityID<UUID>?> = optReference(
        "product_id",
        ProductsTable,
        onDelete = ReferenceOption.SET_NULL
    )
    val number: Column<String> = varchar("number", LARGE_STRING_LENGTH).default("")
    val type: Column<BarcodeType> = enumerationByName("type", FIVE, BarcodeType::class)
    val createdAt: Column<LocalDateTime> = datetime("created_at").default(LocalDateTime.now())
}

class Barcode(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<Barcode>(BarcodesTable)

    var productId by BarcodesTable.productId
    var number by BarcodesTable.number
    var type by BarcodesTable.type
    var createdAt by IngredientsTable.createdAt

    data class Upsert(
        val id: String?,
        val productId: String,
        val number: String,
        val type: BarcodeType,
        val createdAt: String,
    )

    data class Details(
        val id: String,
        val product: Product.Response,
        val number: String,
        val type: BarcodeType,
        val createdAt: LocalDateTime,
    ) {
        companion object {
            fun fromRow(row: Barcode): Details {
                val productDb = Product.findById((row.productId!!))!!

                return Details(
                    id = row.id.toString(),
                    product = Product.Response.fromRow(productDb),
                    number = row.number,
                    type = row.type,
                    createdAt = row.createdAt,
                )
            }
        }
    }

    data class Response(
        val id: String,
        val number: String,
    ) {
        companion object {
            fun fromRow(row: Barcode): Response {
                return Response(
                    id = row.id.toString(),
                    number = row.number,
                )
            }
        }
    }
}
