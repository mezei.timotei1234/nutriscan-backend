package com.tpnlabs.labellingo.models.product

import com.tpnlabs.labellingo.config.DbContstants.LARGE_STRING_LENGTH
import com.tpnlabs.labellingo.config.DbContstants.MEDIUM_STRING_LENGTH
import com.tpnlabs.labellingo.config.DbContstants.STRING_LENGTH
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.LocalDateTime
import java.util.*

object DistributorsTable : UUIDTable("distributors") {
    val name: Column<String> = varchar("name", LARGE_STRING_LENGTH).default("")
    val city: Column<String> = varchar("city", MEDIUM_STRING_LENGTH).default("")
    val county: Column<String> = varchar("county", MEDIUM_STRING_LENGTH).default("")
    val country: Column<String> = varchar("country", MEDIUM_STRING_LENGTH).default("")
    val street: Column<String> = varchar("street", LARGE_STRING_LENGTH).default("")
    val phone: Column<String> = varchar("phone", STRING_LENGTH).default("")
    val email: Column<String> = varchar("email", LARGE_STRING_LENGTH).default("")
    val website: Column<String> = varchar("website", LARGE_STRING_LENGTH).default("")
    val createdAt: Column<LocalDateTime> = datetime("created_at").default(LocalDateTime.now())
}

class Distributor(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<Distributor>(DistributorsTable)

    var name by DistributorsTable.name
    var city by DistributorsTable.city
    var county by DistributorsTable.county
    var country by DistributorsTable.country
    var street by DistributorsTable.street
    var phone by DistributorsTable.phone
    var email by DistributorsTable.email
    var website by DistributorsTable.website
    var createdAt by DistributorsTable.createdAt

    data class Upsert(
        val id: String?,
        val name: String,
        val city: String,
        val county: String,
        val country: String,
        val street: String,
        val phone: String,
        val email: String,
        val website: String,
    )

    data class Details(
        val id: String,
        val name: String,
        val city: String,
        val county: String,
        val country: String,
        val street: String,
        val phone: String,
        val email: String,
        val website: String,
        val createdAt: LocalDateTime,
    ) {
        companion object {
            fun fromRow(row: Distributor): Details {
                return Details(
                    id = row.id.toString(),
                    name = row.name,
                    city = row.city,
                    county = row.county,
                    country = row.country,
                    street = row.street,
                    phone = row.phone,
                    email = row.email,
                    website = row.website,
                    createdAt = row.createdAt,
                )
            }
        }
    }

    data class Response(
        val id: String,
        val name: String,
        val country: String,
    ) {
        companion object {
            fun fromRow(row: Distributor): Response = Response(
                id = row.id.toString(),
                name = row.name,
                country = row.country,
            )
        }
    }
}
