package com.tpnlabs.labellingo.database

import com.tpnlabs.labellingo.config.DbContstants
import com.tpnlabs.labellingo.config.ENV_MODE
import com.tpnlabs.labellingo.config.Environment
import com.tpnlabs.labellingo.models.log.AuthLogsTable
import com.tpnlabs.labellingo.models.product.BarcodesTable
import com.tpnlabs.labellingo.models.product.DistributorsTable
import com.tpnlabs.labellingo.models.product.ImagesTable
import com.tpnlabs.labellingo.models.product.IngredientsTable
import com.tpnlabs.labellingo.models.product.MacroNutrientsTable
import com.tpnlabs.labellingo.models.product.MicroNutrientsTable
import com.tpnlabs.labellingo.models.product.ProductsTable
import com.tpnlabs.labellingo.models.user.PreferencesTable
import com.tpnlabs.labellingo.models.user.UsersTable
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory

data class PaginatedResult<T>(
    val items: List<T>,
    val total: Int
)

interface IDatabaseFactory {
    fun init()
    suspend fun <T> dbQuery(block: () -> T): T
}

data class DatabaseCredentials(
    val dbHost: String,
    val dbUser: String,
    val dbPass: String,
    val driverClassName: String,
    val envJdbcUrl: String,
)

class DatabaseFactory : IDatabaseFactory {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val credentials: DatabaseCredentials = when (ENV_MODE) {
        Environment.UNKNOWN -> DatabaseCredentials(
            "",
            "sa",
            "",
            "org.h2.Driver",
            "jdbc:h2:mem:demo;MODE=MySQL"
        )
        else -> DatabaseCredentials(
            System.getenv("DB_HOST") ?: "127.0.0.1",
            System.getenv("DB_USER") ?: "postgres",
            System.getenv("DB_PASS").orEmpty(),
            "org.postgresql.Driver",
            "jdbc:postgresql://${System.getenv("DB_HOST")}:${System.getenv("DB_PORT")}/${System.getenv("DB_NAME")}"
        )
    }

    private fun hikari(): HikariDataSource {
        logger.debug("[DB] Connecting to ${credentials.envJdbcUrl}")

        val config = HikariConfig().apply {
            driverClassName = credentials.driverClassName
            jdbcUrl = credentials.envJdbcUrl
            username = credentials.dbUser
            password = credentials.dbPass
            maximumPoolSize = DbContstants.MAX_POOL_SIZE
            minimumIdle = 1
            connectionTimeout = DbContstants.CONN_TIMEOUT
            leakDetectionThreshold = (DbContstants.LEAK_TIMES * DbContstants.LEAK_THRESHOLD).toLong()
            isAutoCommit = false
            transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        }
        return HikariDataSource(config)
    }

    override fun init() {
        Database.connect(hikari())
        transaction {
            SchemaUtils.create(
                AuthLogsTable,
                PreferencesTable,
                DistributorsTable,
                ImagesTable,
                IngredientsTable,
                BarcodesTable,
                MicroNutrientsTable,
                MacroNutrientsTable,
                ProductsTable,
                UsersTable,
            )
        }
    }

    override suspend fun <T> dbQuery(block: () -> T): T = withContext(Dispatchers.IO) {
        transaction { block() }
    }
}
