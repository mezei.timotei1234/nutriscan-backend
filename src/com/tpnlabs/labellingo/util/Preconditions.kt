package com.tpnlabs.labellingo.util

import com.tpnlabs.labellingo.config.EMAIL_ADDRESS_PATTERN
import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.user.PreferenceName
import com.tpnlabs.labellingo.models.user.UsersTable
import org.jetbrains.exposed.sql.select
import org.slf4j.LoggerFactory
import java.util.UUID

class Preconditions(private val client: IDatabaseFactory) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    suspend fun checkIfUserExists(userId: String): Boolean {
        logger.debug("Checking if user exists: $userId")
        return client.dbQuery {
            val userInDatabase = UsersTable.select { (UsersTable.id eq UUID.fromString(userId)) }.firstOrNull()
            userInDatabase != null
        }
    }

    fun checkIfEmailIsValid(email: String): Boolean {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches()
    }

    suspend fun checkIfEmailExists(email: String): Boolean {
        return client.dbQuery {
            val userInDatabase = UsersTable.select { (UsersTable.email eq email) }.firstOrNull()
            userInDatabase != null
        }
    }

    fun checkIfPreferenceExists(preferenceName: String): Boolean {
        logger.debug("Checking if preference exists: $preferenceName")
        return PreferenceName.values().any { it.toString() == preferenceName }
    }

    fun checkIfUsernameContainsOnlyLetters(username: String): Boolean {
        return username.all { it.isLetter() }
    }

    fun checkIfStudentMeetingsIsValid(totalMeetings: Int): Boolean {
        return totalMeetings > 0
    }

    suspend fun checkIfUsernameExists(username: String): Boolean {
        return client.dbQuery {
            val userInDatabase = UsersTable.select { (UsersTable.username eq username.lowercase()) }.firstOrNull()
            userInDatabase != null
        }
    }
}
