package com.tpnlabs.labellingo.util

import com.auth0.jwt.JWT
import com.auth0.jwt.exceptions.SignatureVerificationException
import com.auth0.jwt.exceptions.TokenExpiredException
import com.auth0.jwt.interfaces.DecodedJWT
import com.tpnlabs.labellingo.config.APIConstants
import com.tpnlabs.labellingo.config.APIConstants.JWT_ALGORITHM
import com.tpnlabs.labellingo.config.APIConstants.JWT_ISSUER
import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.user.User
import io.ktor.server.application.createRouteScopedPlugin
import io.ktor.server.application.install
import io.ktor.server.auth.AuthenticationChecked
import io.ktor.server.request.authorization
import io.ktor.server.routing.Route
import io.ktor.server.routing.RouteSelector
import io.ktor.server.routing.RouteSelectorEvaluation
import io.ktor.server.routing.RoutingResolveContext
import java.util.*

fun getUserRoleFromJWT(token: String): Int {
    try {
        val dataJwt: DecodedJWT = JWT.require(JWT_ALGORITHM)
            .withIssuer(JWT_ISSUER)
            .build()
            .verify(token)
        return dataJwt.getClaim("role").asInt()
    } catch (ex: SignatureVerificationException) {
        throw JwtSignatureFails(ex.toString())
    } catch (ex: TokenExpiredException) {
        throw JwtSignatureFails(ex.toString())
    }
}

fun getUserIdFromJWT(token: String): String {
    try {
        val dataJwt: DecodedJWT = JWT.require(JWT_ALGORITHM)
            .withIssuer(JWT_ISSUER)
            .build()
            .verify(token)
        return dataJwt.getClaim("id").asString()
    } catch (ex: SignatureVerificationException) {
        throw JwtSignatureFails(ex.toString())
    } catch (ex: TokenExpiredException) {
        throw JwtSignatureFails(ex.toString())
    }
}

fun isAdmin(token: String): Boolean {
    return getUserRoleFromJWT(token) == APIConstants.ADMIN_ROLE
}

suspend fun isEmailValidated(token: String, client: IDatabaseFactory): Boolean {
    val userId = getUserIdFromJWT(token)
    return client.dbQuery {
        val user = User.findById(UUID.fromString(userId))
        user!!.emailVerifiedAt != null
    }
}

fun Route.admin(build: Route.() -> Unit): Route {
    val route = createChild(CustomSelector())
    val plugin = createRouteScopedPlugin("CustomAuthorization") {
        on(AuthenticationChecked) { call ->
            if (call.request.authorization() == null) {
                throw UserNotAuthenticated()
            }
            val token = call.request.authorization()?.removePrefix("Bearer ")!!
            if (!isAdmin(token)) {
                val userId = getUserIdFromJWT(token)
                throw UserNotAdmin(userId)
            }
        }
    }
    route.install(plugin)
    route.build()
    return route
}

fun Route.userValidated(client: IDatabaseFactory, build: Route.() -> Unit): Route {
    val route = createChild(CustomSelector())
    val plugin = createRouteScopedPlugin("CustomAuthorization") {
        on(AuthenticationChecked) { call ->
            if (call.request.authorization() == null) {
                throw UserNotAuthenticated()
            }
            val token = call.request.authorization()?.removePrefix("Bearer ")!!
            if (!isEmailValidated(token, client)) {
                val userId = getUserIdFromJWT(token)
                throw UserEmailNotVerified(userId)
            }
        }
    }
    route.install(plugin)
    route.build()
    return route
}

private class CustomSelector : RouteSelector() {
    override fun evaluate(context: RoutingResolveContext, segmentIndex: Int) = RouteSelectorEvaluation.Transparent
}
