package com.tpnlabs.labellingo.util.exceptions.product

import com.tpnlabs.labellingo.util.ApiException
import io.ktor.http.HttpStatusCode

class MacroNutrientNotFound(id: String) :
    ApiException("MacroNutrient is invalid", "MacroNutrient is invalid $id", HttpStatusCode.BadRequest)
