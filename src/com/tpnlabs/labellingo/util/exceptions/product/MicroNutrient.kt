package com.tpnlabs.labellingo.util.exceptions.product

import com.tpnlabs.labellingo.util.ApiException
import io.ktor.http.HttpStatusCode

class MicroNutrientNotFound(id: String) :
    ApiException("MicroNutrient is invalid", "MicroNutrient is invalid $id", HttpStatusCode.BadRequest)
