package com.tpnlabs.labellingo.util.exceptions.product

import com.tpnlabs.labellingo.util.ApiException
import io.ktor.http.HttpStatusCode

class ProductNotFound(id: String) :
    ApiException("Product is invalid", "Product is invalid $id", HttpStatusCode.BadRequest)
