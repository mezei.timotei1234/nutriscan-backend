package com.tpnlabs.labellingo.util.exceptions.product

import com.tpnlabs.labellingo.util.ApiException
import io.ktor.http.HttpStatusCode

class BarcodeNotFound(id: String) :
    ApiException("Barcode is invalid", "Barcode is invalid $id", HttpStatusCode.BadRequest)
