package com.tpnlabs.labellingo.util.exceptions.product

import com.tpnlabs.labellingo.util.ApiException
import io.ktor.http.HttpStatusCode

class DistributorNotFound(id: String) :
    ApiException("Distributor is invalid", "Distributor is invalid $id", HttpStatusCode.BadRequest)
