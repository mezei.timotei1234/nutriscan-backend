package com.tpnlabs.labellingo.util.exceptions.product

import com.tpnlabs.labellingo.util.ApiException
import io.ktor.http.HttpStatusCode

class ImageNotFound(id: String) :
    ApiException("Image is invalid", "Image is invalid $id", HttpStatusCode.BadRequest)
