package com.tpnlabs.labellingo.util.exceptions.product

import com.tpnlabs.labellingo.util.ApiException
import io.ktor.http.HttpStatusCode

class IngredientNotFound(id: String) :
    ApiException("Ingredient is invalid", "Ingredient is invalid $id", HttpStatusCode.BadRequest)
