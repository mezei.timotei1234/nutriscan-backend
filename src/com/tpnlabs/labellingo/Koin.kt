package com.tpnlabs.labellingo

import com.tpnlabs.labellingo.database.DatabaseFactory
import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.service.product.BarcodeService
import com.tpnlabs.labellingo.service.product.DistributorService
import com.tpnlabs.labellingo.service.product.IBarcodeService
import com.tpnlabs.labellingo.service.product.IDistributorService
import com.tpnlabs.labellingo.service.product.IImageService
import com.tpnlabs.labellingo.service.product.IIngredientService
import com.tpnlabs.labellingo.service.product.IMacroNutrientService
import com.tpnlabs.labellingo.service.product.IMicroNutrientService
import com.tpnlabs.labellingo.service.product.IProductService
import com.tpnlabs.labellingo.service.product.ImageService
import com.tpnlabs.labellingo.service.product.IngredientService
import com.tpnlabs.labellingo.service.product.MacroNutrientService
import com.tpnlabs.labellingo.service.product.MicroNutrientService
import com.tpnlabs.labellingo.service.product.ProductService
import com.tpnlabs.labellingo.service.user.IPreferenceService
import com.tpnlabs.labellingo.service.user.IUserService
import com.tpnlabs.labellingo.service.user.PreferenceService
import com.tpnlabs.labellingo.service.user.UserService
import org.koin.dsl.module

val serviceKoinModule = module {
    single<IBarcodeService> { BarcodeService(get()) }
    single<IDistributorService> { DistributorService(get()) }
    single<IProductService> { ProductService(get()) }
    single<IIngredientService> { IngredientService(get()) }
    single<IMacroNutrientService> { MacroNutrientService(get()) }
    single<IMicroNutrientService> { MicroNutrientService(get()) }
    single<IImageService> { ImageService(get()) }
    single<IUserService> { UserService(get()) }
    single<IPreferenceService> { PreferenceService(get()) }
}

val databaseKoinModule = module {
    single<IDatabaseFactory> { DatabaseFactory() }
}
