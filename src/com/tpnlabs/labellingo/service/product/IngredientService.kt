package com.tpnlabs.labellingo.service.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Ingredient
import com.tpnlabs.labellingo.util.exceptions.product.IngredientNotFound
import java.time.LocalDateTime
import java.util.UUID

interface IIngredientService {
    suspend fun userRead(barcode: String): Ingredient.Details
    suspend fun userCreate(props: Ingredient.Upsert): Ingredient.Response
}

class IngredientService(private val databaseFactory: IDatabaseFactory) : IIngredientService {
    private fun getIngredient(id: String) = Ingredient.findById(UUID.fromString(id)) ?: throw IngredientNotFound(id)
    override suspend fun userRead(id: String): Ingredient.Details {
        return databaseFactory.dbQuery {
            getIngredient(id).let { Ingredient.Details.fromRow(it) }
        }
    }

    override suspend fun userCreate(props: Ingredient.Upsert): Ingredient.Response {
        val ingredient = databaseFactory.dbQuery {
            Ingredient.new {
                name = props.name
                createdAt = LocalDateTime.now()
            }
        }
        return Ingredient.Response.fromRow(ingredient)
    }
}
