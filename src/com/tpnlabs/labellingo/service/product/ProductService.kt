package com.tpnlabs.labellingo.service.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Product
import com.tpnlabs.labellingo.util.exceptions.product.ProductNotFound
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.UUID

interface IProductService {
    suspend fun userRead(barcode: String): Product.Details
    suspend fun userCreate(props: Product.Upsert): Product.Response
}

class ProductService(private val databaseFactory: IDatabaseFactory) : IProductService {
    private fun getProduct(id: String) = Product.findById(UUID.fromString(id)) ?: throw ProductNotFound(id)
    override suspend fun userRead(id: String): Product.Details {
        return databaseFactory.dbQuery {
            getProduct(id).let { Product.Details.fromRow(it) }
        }
    }

    override suspend fun userCreate(props: Product.Upsert): Product.Response {
        val product = databaseFactory.dbQuery {
            Product.new {
                name = props.name
                brandName = props.brandName
                barcode = props.barcode
                weightValue = props.weightValue
                weightType = props.weightType
                producedAt = LocalDate.parse(props.producedAt)
                expiresAt = LocalDate.parse(props.expiresAt)
                createdAt = LocalDateTime.now()
            }
        }
        return Product.Response.fromRow(product)
    }
}
