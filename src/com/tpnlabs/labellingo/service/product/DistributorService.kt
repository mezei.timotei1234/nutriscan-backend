package com.tpnlabs.labellingo.service.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Distributor
import com.tpnlabs.labellingo.util.exceptions.product.DistributorNotFound
import java.time.LocalDateTime
import java.util.UUID

interface IDistributorService {
    suspend fun userRead(id: String): Distributor.Details
    suspend fun userCreate(props: Distributor.Upsert): Distributor.Response
}

class DistributorService(private val databaseFactory: IDatabaseFactory) : IDistributorService {

    private fun getDistributor(id: String) = Distributor.findById(UUID.fromString(id)) ?: throw DistributorNotFound(id)
    override suspend fun userRead(id: String): Distributor.Details {
        return databaseFactory.dbQuery {
            getDistributor(id).let { Distributor.Details.fromRow(it) }
        }
    }

    override suspend fun userCreate(props: Distributor.Upsert): Distributor.Response {
        val distributor = databaseFactory.dbQuery {
            Distributor.new {
                name = props.name
                city = props.city
                county = props.county
                country = props.country
                street = props.street
                phone = props.phone
                email = props.email
                website = props.website
                createdAt = LocalDateTime.now()
            }
        }
        return Distributor.Response.fromRow(distributor)
    }
}
