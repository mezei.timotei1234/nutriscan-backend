package com.tpnlabs.labellingo.service.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.MacroNutrient
import com.tpnlabs.labellingo.models.product.MicroNutrient
import com.tpnlabs.labellingo.util.exceptions.product.MacroNutrientNotFound
import com.tpnlabs.labellingo.util.exceptions.product.MicroNutrientNotFound
import java.time.LocalDateTime
import java.util.UUID
interface IMicroNutrientService {
    suspend fun userRead(barcode: String): MicroNutrient.Details
    suspend fun userCreate(props: MicroNutrient.Upsert): MicroNutrient.Response
}

class MicroNutrientService(private val databaseFactory: IDatabaseFactory) : IMicroNutrientService {
    private fun getMicroNutrient(id: String) =
        MicroNutrient.findById(UUID.fromString(id)) ?: throw MicroNutrientNotFound(id)
    override suspend fun userRead(id: String): MicroNutrient.Details {
        return databaseFactory.dbQuery {
            getMicroNutrient(id).let { MicroNutrient.Details.fromRow(it) }
        }
    }

    override suspend fun userCreate(props: MicroNutrient.Upsert): MicroNutrient.Response {
        val microNutrient = databaseFactory.dbQuery {
            MicroNutrient.new {
                vitamins = props.vitamins
                minerals = props.minerals
                createdAt = LocalDateTime.now()
            }
        }
        return MicroNutrient.Response.fromRow(microNutrient)
    }
}
