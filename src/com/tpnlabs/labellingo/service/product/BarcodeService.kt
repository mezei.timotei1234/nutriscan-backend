package com.tpnlabs.labellingo.service.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Barcode
import com.tpnlabs.labellingo.util.exceptions.product.BarcodeNotFound
import java.time.LocalDateTime
import java.util.UUID

interface IBarcodeService {
    suspend fun userRead(barcode: String): Barcode.Details
    suspend fun userCreate(props: Barcode.Upsert): Barcode.Response
}

class BarcodeService(private val databaseFactory: IDatabaseFactory) : IBarcodeService {
    private fun getBarcode(id: String) = Barcode.findById(UUID.fromString(id)) ?: throw BarcodeNotFound(id)
    override suspend fun userRead(id: String): Barcode.Details {
        return databaseFactory.dbQuery {
            getBarcode(id).let { Barcode.Details.fromRow(it) }
        }
    }

    override suspend fun userCreate(props: Barcode.Upsert): Barcode.Response {
        val barcode = databaseFactory.dbQuery {
            Barcode.new {
                number = props.number
                type = props.type
                createdAt = LocalDateTime.now()
            }
        }
        return Barcode.Response.fromRow(barcode)
    }
}
