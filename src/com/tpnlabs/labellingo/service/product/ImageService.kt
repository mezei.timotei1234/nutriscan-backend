package com.tpnlabs.labellingo.service.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Image
import com.tpnlabs.labellingo.util.exceptions.product.ImageNotFound
import java.time.LocalDateTime
import java.util.UUID

interface IImageService {
    suspend fun userRead(barcode: String): Image.Details
    suspend fun userCreate(props: Image.Upsert): Image.Response
}

class ImageService(private val databaseFactory: IDatabaseFactory) : IImageService {
    private fun getImage(id: String) = Image.findById(UUID.fromString(id)) ?: throw ImageNotFound(id)
    override suspend fun userRead(barcode: String): Image.Details {
        return databaseFactory.dbQuery {
            getImage(barcode).let { Image.Details.fromRow(it) }
        }
    }

    override suspend fun userCreate(props: Image.Upsert): Image.Response {
        val image = databaseFactory.dbQuery {
            Image.new {
                imageData = props.imageData
                imageType = props.imageType
                createdAt = LocalDateTime.now()
            }
        }
        return Image.Response.fromRow(image)
    }
}
