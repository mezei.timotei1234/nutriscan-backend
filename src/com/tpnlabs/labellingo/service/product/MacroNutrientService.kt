package com.tpnlabs.labellingo.service.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.MacroNutrient
import com.tpnlabs.labellingo.util.exceptions.product.MacroNutrientNotFound
import java.time.LocalDateTime
import java.util.UUID
interface IMacroNutrientService {
    suspend fun userRead(barcode: String): MacroNutrient.Details
    suspend fun userCreate(props: MacroNutrient.Upsert): MacroNutrient.Response
}

class MacroNutrientService(private val databaseFactory: IDatabaseFactory) : IMacroNutrientService {
    private fun getMacroNutrient(id: String) =
        MacroNutrient.findById(UUID.fromString(id)) ?: throw MacroNutrientNotFound(id)
    override suspend fun userRead(id: String): MacroNutrient.Details {
        return databaseFactory.dbQuery {
            getMacroNutrient(id).let { MacroNutrient.Details.fromRow(it) }
        }
    }

    override suspend fun userCreate(props: MacroNutrient.Upsert): MacroNutrient.Response {
        val macroNutrient = databaseFactory.dbQuery {
            MacroNutrient.new {
                carbohydrates = props.carbohydrates
                energyKcal = props.energyKcal
                energyKJ = props.energyKJ
                fat = props.fat
                proteins = props.proteins
                salt = props.salt
                saturatedFats = props.saturatedFats
                sugars = props.sugars
                createdAt = LocalDateTime.now()
            }
        }
        return MacroNutrient.Response.fromRow(macroNutrient)
    }
}
