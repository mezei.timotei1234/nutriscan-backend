package com.tpnlabs.labellingo.api.user

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.user.Preference
import com.tpnlabs.labellingo.models.user.PreferenceName
import com.tpnlabs.labellingo.service.user.IPreferenceService
import com.tpnlabs.labellingo.util.userValidated
import io.ktor.server.application.call
import io.ktor.server.request.authorization
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.put
import io.ktor.server.routing.route

fun Route.preferenceApi(preferenceService: IPreferenceService, databaseFactory: IDatabaseFactory) {
    userValidated(databaseFactory) {
        route("/preferences") {
            get {
                val token = call.request.authorization()?.removePrefix("Bearer ")!!
                val preferences = preferenceService.userAll(token)
                call.respond(preferences)
            }

            post {
                val token = call.request.authorization()?.removePrefix("Bearer ")!!
                val newPreference = call.receive<Preference.PreferenceNew>()
                val preference = preferenceService.userCreate(token, newPreference)
                call.respond(preference)
            }

            put {
                val token = call.request.authorization()?.removePrefix("Bearer ")!!
                val newPreference = call.receive<Preference.PreferenceNew>()
                val preference = preferenceService.userUpdate(token, newPreference)
                call.respond(preference)
            }

            get("/{name}") {
                val token = call.request.authorization()?.removePrefix("Bearer ")!!
                val preferenceName = PreferenceName.valueOf(call.parameters["name"]!!.uppercase())
                val preference = preferenceService.userRead(token, preferenceName)
                call.respond(preference)
            }
        }
    }
}
