package com.tpnlabs.labellingo.api.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Barcode
import com.tpnlabs.labellingo.service.product.IBarcodeService
import com.tpnlabs.labellingo.util.userValidated
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route

fun Route.barcodeApi(barcodeService: IBarcodeService, databaseFactory: IDatabaseFactory) {
    userValidated(databaseFactory) {
        route("/barcodes") {
            get("/{id}") {
                val barcode = barcodeService.userRead(call.parameters["id"]!!)
                call.respond(barcode)
            }

            post {
                val newBarcode = call.receive<Barcode.Upsert>()
                val preference = barcodeService.userCreate(newBarcode)
                call.respond(preference)
            }
        }
    }
}
