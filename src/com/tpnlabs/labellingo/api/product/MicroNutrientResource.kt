package com.tpnlabs.labellingo.api.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.MicroNutrient
import com.tpnlabs.labellingo.service.product.IMicroNutrientService
import com.tpnlabs.labellingo.util.userValidated
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route

fun Route.microNutrientApi(microNutrientService: IMicroNutrientService, databaseFactory: IDatabaseFactory) {
    userValidated(databaseFactory) {
        route("/micronutrients") {
            get("/{id}") {
                val microNutrient = microNutrientService.userRead(call.parameters["id"]!!)
                call.respond(microNutrient)
            }

            post {
                val newMicroNutrient = call.receive<MicroNutrient.Upsert>()
                val preference = microNutrientService.userCreate(newMicroNutrient)
                call.respond(preference)
            }
        }
    }
}
