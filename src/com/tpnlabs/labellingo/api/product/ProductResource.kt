package com.tpnlabs.labellingo.api.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Product
import com.tpnlabs.labellingo.service.product.IProductService
import com.tpnlabs.labellingo.util.userValidated
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route

fun Route.productApi(productService: IProductService, databaseFactory: IDatabaseFactory) {
    userValidated(databaseFactory) {
        route("/products") {
            get("/{id}") {
                val product = productService.userRead(call.parameters["id"]!!)
                call.respond(product)
            }

            post {
                val newProduct = call.receive<Product.Upsert>()
                val preference = productService.userCreate(newProduct)
                call.respond(preference)
            }
        }
    }
}
