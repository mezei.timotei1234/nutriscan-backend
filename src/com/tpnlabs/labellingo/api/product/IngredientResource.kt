package com.tpnlabs.labellingo.api.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Ingredient
import com.tpnlabs.labellingo.service.product.IIngredientService
import com.tpnlabs.labellingo.util.userValidated
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route

fun Route.ingredientApi(ingredientService: IIngredientService, databaseFactory: IDatabaseFactory) {
    userValidated(databaseFactory) {
        route("/ingridients") {
            get("/{id}") {
                val ingredient = ingredientService.userRead(call.parameters["id"]!!)
                call.respond(ingredient)
            }

            post {
                val newIngredient = call.receive<Ingredient.Upsert>()
                val preference = ingredientService.userCreate(newIngredient)
                call.respond(preference)
            }
        }
    }
}
