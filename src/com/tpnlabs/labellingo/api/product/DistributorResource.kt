package com.tpnlabs.labellingo.api.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Distributor
import com.tpnlabs.labellingo.service.product.IDistributorService
import com.tpnlabs.labellingo.util.userValidated
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route

fun Route.distributorApi(distributorService: IDistributorService, databaseFactory: IDatabaseFactory) {
    userValidated(databaseFactory) {
        route("/distributors") {
            get("/{id}") {
                val distributor = distributorService.userRead(call.parameters["id"]!!)
                call.respond(distributor)
            }

            get("/products") { }

            post {
                val newDistributor = call.receive<Distributor.Upsert>()
                val preference = distributorService.userCreate(newDistributor)
                call.respond(preference)
            }
        }
    }
}
