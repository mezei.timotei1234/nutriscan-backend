package com.tpnlabs.labellingo.api.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.Image
import com.tpnlabs.labellingo.models.product.Product
import com.tpnlabs.labellingo.service.product.IImageService
import com.tpnlabs.labellingo.service.product.IProductService
import com.tpnlabs.labellingo.util.userValidated
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route

fun Route.imageApi(imageService: IImageService, databaseFactory: IDatabaseFactory) {
    userValidated(databaseFactory) {
        route("/images") {
            get("/{id}") {
                val image = imageService.userRead(call.parameters["id"]!!)
                call.respond(image)
            }

            post {
                val newImage = call.receive<Image.Upsert>()
                val preference = imageService.userCreate(newImage)
                call.respond(preference)
            }
        }
    }
}
