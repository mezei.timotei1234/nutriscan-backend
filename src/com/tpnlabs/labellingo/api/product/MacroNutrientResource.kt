package com.tpnlabs.labellingo.api.product

import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.models.product.MacroNutrient
import com.tpnlabs.labellingo.service.product.IMacroNutrientService
import com.tpnlabs.labellingo.util.userValidated
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.route

fun Route.macroNutrientApi(macroNutrientService: IMacroNutrientService, databaseFactory: IDatabaseFactory) {
    userValidated(databaseFactory) {
        route("/macronutrients") {
            get("/{id}") {
                val macroNutrient = macroNutrientService.userRead(call.parameters["id"]!!)
                call.respond(macroNutrient)
            }

            post {
                val newMacroNutrient = call.receive<MacroNutrient.Upsert>()
                val preference = macroNutrientService.userCreate(newMacroNutrient)
                call.respond(preference)
            }
        }
    }
}
