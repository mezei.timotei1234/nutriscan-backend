package com.tpnlabs.labellingo.config

import com.tpnlabs.labellingo.util.ApiException
import io.ktor.http.HttpStatusCode
import io.ktor.server.plugins.statuspages.StatusPagesConfig
import io.ktor.server.response.respond
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger("StatusPages")

fun StatusPagesConfig.statusPages() {
    exception<ApiException> { call, cause ->
        logger.error("Code/Exception:[${cause.statusCode.value}][${cause.message}]")
        call.respond(
            cause.statusCode, mapOf(
                "message" to cause.clientMessage,
                "code" to cause.statusCode.value.toString()
            )
        )
    }
    exception<Exception> { call, cause ->
        logger.error("Internal exception/ex:${cause}")
        call.respond(
            HttpStatusCode.InternalServerError, mapOf(
                "message" to "Internal Server Error",
                "code" to "500",
            )
        )
    }
}
