package com.tpnlabs.labellingo.config

enum class Environment(val code: String) {
    PROD("PROD"),
    BETA("BETA"),
    LOCAL("LOCAL"),
    UNKNOWN("UNKNOWN"),
}

val ENV_MODE = when (System.getenv("ENV_MODE")) {
    Environment.PROD.code -> Environment.PROD
    Environment.BETA.code -> Environment.BETA
    Environment.LOCAL.code -> Environment.LOCAL
    else -> Environment.UNKNOWN
}
