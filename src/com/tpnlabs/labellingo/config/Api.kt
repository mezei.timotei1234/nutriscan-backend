package com.tpnlabs.labellingo.config

import com.tpnlabs.labellingo.api.product.distributorApi
import com.tpnlabs.labellingo.api.user.preferenceApi
import com.tpnlabs.labellingo.api.user.usersApi
import com.tpnlabs.labellingo.database.IDatabaseFactory
import com.tpnlabs.labellingo.service.product.IDistributorService
import com.tpnlabs.labellingo.service.user.IPreferenceService
import com.tpnlabs.labellingo.service.user.IUserService
import io.ktor.server.routing.Route
import io.ktor.server.routing.route
import org.koin.ktor.ext.inject

fun Route.api() {
    val dbService: IDatabaseFactory by inject()

    val distributorService: IDistributorService by inject()
    val preferenceService: IPreferenceService by inject()
    val userService: IUserService by inject()

    route("/") {
        distributorApi(distributorService, dbService)
        usersApi(userService)
        preferenceApi(preferenceService, dbService)
    }
}
