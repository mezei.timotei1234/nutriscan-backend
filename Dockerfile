FROM gradle:8-jdk17 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle buildFatJar --no-daemon

FROM openjdk:17
EXPOSE 3000:3000
RUN microdnf install unzip
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/*all.jar /app/tgcp_packaged.jar
ENTRYPOINT ["java","-jar","/app/tgcp_packaged.jar"]